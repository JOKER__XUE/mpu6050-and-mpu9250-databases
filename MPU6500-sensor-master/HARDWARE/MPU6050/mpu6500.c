#include "mpu6500.h"
#include "sys.h"
#include "delay.h"
#include "usart.h"   
#include "log.h"
#include "spi.h"

static void mpu_set_cs(MPU_CS_TypeDef num);
static void mpu_clear_cs(MPU_CS_TypeDef num);

static uint8_t MPU6500_Check(u8 addr)
{
  uint8_t deviceID = ERROR;

  deviceID = MPU_Read_Byte(addr, MPU6500_WHO_AM_I);
  if(deviceID != MPU6500_Device_ID)
    return 1;

  return 0;
}

//初始化MPU6050
//返回值:0,成功
//    其他,错误代码
#define	W25QXX_CS 		PBout(14)  		//W25QXX的片选信号
#define MPU6500_1_CS    PBout(8)
#define MPU6500_2_CS    PBout(9)
#define MPU6500_InitRegNum 12

void MPU_SPI_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
 
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);//使能GPIOB时钟
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG, ENABLE);//使能GPIOG时钟

	  //GPIOB14
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_14;//PB14
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//输出
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100MHz
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
  GPIO_Init(GPIOB, &GPIO_InitStructure);//初始化

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;//PG7
  GPIO_Init(GPIOG, &GPIO_InitStructure);//初始化
 
	GPIO_SetBits(GPIOG,GPIO_Pin_7);//PG7输出1,防止NRF干扰SPI FLASH的通信 
	W25QXX_CS=1;			//SPI FLASH不选中
	MPU6500_1_CS = 1;
	MPU6500_2_CS = 1;
	SPI1_Init();
}

u8 MPU_Init(u8 addr)
{
	u8 err;
	MPU_Write_Byte(addr, MPU6500_PWR_MGMT_1,0X80);	
	delay_ms(1);
	MPU_Write_Byte(addr, MPU6500_USER_CTRL, 0x10); // disable i2c
	delay_ms(1);
	MPU_Write_Byte(addr, MPU6500_FIFO_EN, 0xf8); // enable fifo
	delay_ms(1);
	
	err = MPU6500_Check(addr);
  if(err)
    return 1;
	return 0;
}

//设置MPU6500陀螺仪传感器满量程范围
//fsr:0,±250dps;1,±500dps;2,±1000dps;3,±2000dps
//返回值:0,设置成功
//    其他,设置失败 
u8 MPU_Set_Gyro_Fsr(u8 addr, u8 fsr)
{
	return MPU_Write_Byte(addr, MPU_GYRO_CFG_REG,fsr<<3);//设置陀螺仪满量程范围  
}
//设置MPU6500加速度传感器满量程范围
//fsr:0,±2g;1,±4g;2,±8g;3,±16g
//返回值:0,设置成功
//    其他,设置失败 
u8 MPU_Set_Accel_Fsr(u8 addr, u8 fsr)
{
	return MPU_Write_Byte(addr, MPU_ACCEL_CFG_REG,fsr<<3);//设置加速度传感器满量程范围  
}
//设置MPU6500的数字低通滤波器
//lpf:数字低通滤波频率(Hz)
//返回值:0,设置成功
//    其他,设置失败 
u8 MPU_Set_LPF(u8 addr, u16 lpf)
{
	u8 data=0;
	if(lpf>=188)data=1;
	else if(lpf>=98)data=2;
	else if(lpf>=42)data=3;
	else if(lpf>=20)data=4;
	else if(lpf>=10)data=5;
	else data=6; 
	return MPU_Write_Byte(addr, MPU_CFG_REG,data);//设置数字低通滤波器  
}
//设置MPU6500的采样率(假定Fs=1KHz)
//rate:4~1000(Hz)
//返回值:0,设置成功
//    其他,设置失败 
u8 MPU_Set_Rate(u8 addr, u16 rate)
{
	u8 data;
	if(rate>1000)rate=1000;
	if(rate<4)rate=4;
	data=1000/rate-1;
	data=MPU_Write_Byte(addr, MPU_SAMPLE_RATE_REG,data);	//设置数字低通滤波器
 	return MPU_Set_LPF(addr, rate/2);	//自动设置LPF为采样率的一半
}

//得到温度值
//返回值:温度值(扩大了100倍)
short MPU_Get_Temperature(u8 addr)
{
    u8 buf[2]; 
    short raw;
	float temp;
	MPU_Read_Len(addr,MPU_TEMP_OUTH_REG,2,buf); 
    raw=((u16)buf[0]<<8)|buf[1];  
    temp=36.53+((double)raw)/340;  
    return temp*100;;
}
//得到陀螺仪值(原始值)
//gx,gy,gz:陀螺仪x,y,z轴的原始读数(带符号)
//返回值:0,成功
//    其他,错误代码
u8 MPU_Get_Gyroscope(u8 addr, short *gx,short *gy,short *gz)
{
    u8 buf[6],res;  
	res=MPU_Read_Len(addr,MPU_GYRO_XOUTH_REG,6,buf);
	if(res==0)
	{
		*gx=((u16)buf[0]<<8)|buf[1];  
		*gy=((u16)buf[2]<<8)|buf[3];  
		*gz=((u16)buf[4]<<8)|buf[5];
	} 	
    return res;;
}
//得到加速度值(原始值)
//gx,gy,gz:陀螺仪x,y,z轴的原始读数(带符号)
//返回值:0,成功
//    其他,错误代码
u8 MPU_Get_Accelerometer(u8 addr, short *ax,short *ay,short *az)
{
    u8 buf[6],res;  
	res=MPU_Read_Len(addr,MPU_ACCEL_XOUTH_REG,6,buf);
	if(res==0)
	{
		*ax=((u16)buf[0]<<8)|buf[1];  
		*ay=((u16)buf[2]<<8)|buf[3];  
		*az=((u16)buf[4]<<8)|buf[5];
	} 	
    return res;;
}

//连续写
//addr:器件地址 
//reg:寄存器地址
//len:写入长度
//buf:数据区
//返回值:0,正常
//    其他,错误代码
u8 MPU_Write_Len(u8 addr,u8 reg,u8 len,u8 *buf)
{
	u8 i;
	if(addr >= MPU_CS_NUM_MAX)
		return -1;
	
	mpu_set_cs(addr);

	SPI1_ReadWriteByte(reg);
	for(i=0;i<len;i++)
	{
		SPI1_ReadWriteByte(buf[i]);
	}

	mpu_clear_cs(addr);
	return 0;
} 

//连续读
//addr:器件地址
//reg:要读取的寄存器地址
//len:要读取的长度
//buf:读取到的数据存储区
//返回值:0,正常
//    其他,错误代码
u8 MPU_Read_Len(u8 addr,u8 reg,u8 len,u8 *buf)
{ 
	u8 i;
	if(addr >= MPU_CS_NUM_MAX)
		return -1;
	
	mpu_set_cs(addr);

	SPI1_ReadWriteByte(reg|0x80);
	for(i=0;i<len;i++)
	{
		buf[i] = SPI1_ReadWriteByte(0xff);
	}

	mpu_clear_cs(addr);
	return 0;
}

//写一个字节 
//reg:寄存器地址
//data:数据
//返回值:0,正常
//    其他,错误代码
u8 MPU_Write_Byte(u8 addr, u8 reg,u8 data) 				 
{ 
	if(addr >= MPU_CS_NUM_MAX)
		return -1;
	
	mpu_set_cs(addr);

	SPI1_ReadWriteByte(reg);
	SPI1_ReadWriteByte(data);

	mpu_clear_cs(addr);
	return 0;
}

//读一个字节 
//reg:寄存器地址 
//返回值:读到的数据
u8 MPU_Read_Byte(u8 addr, u8 reg)
{
	u8 readdata;
	if(addr >= MPU_CS_NUM_MAX)
		return -1;
	
	mpu_set_cs(addr);
	SPI1_ReadWriteByte(reg|0x80);
	readdata = SPI1_ReadWriteByte(0x00);
	mpu_clear_cs(addr);
	return readdata;
}

void mget_ms(unsigned long *time)
{

}

int _MLPrintLog (int priority, const char* tag, const char* fmt, ...)
{
	return 0;
}

static void mpu_set_cs(MPU_CS_TypeDef num)
{
	switch(num){
		case MPU_CS_NUM_1:
			MPU6500_1_CS = 0;
			break;
		case MPU_CS_NUM_2:
			MPU6500_2_CS = 0;
			break;
		default:
			return;
	}
}

static void mpu_clear_cs(MPU_CS_TypeDef num)
{
	switch(num){
		case MPU_CS_NUM_1:
			MPU6500_1_CS = 1;
			break;
		case MPU_CS_NUM_2:
			MPU6500_2_CS = 1;
			break;
		default:
			return;
	}
}

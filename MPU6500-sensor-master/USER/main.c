#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "led.h"
#include "key.h"
#include "lcd.h"
#include "mpu6500.h"
#include "inv_mpu.h"
#include "inv_mpu_dmp_motion_driver.h" 
#include "mpl.h"
#include "quaternion_supervisor.h"
#include "fusion_9axis.h"
#include "fast_no_motion.h"
#include "gyro_tc.h"
#include "eMPL_outputs.h"
#include "data_builder.h"
#include "ml_math_func.h"
#include <math.h>


unsigned char *mpl_key = (unsigned char*)"eMPL 5.1";

uint8_t mpu_dmp_init(void);
uint8_t mpu_dmp_get_data(unsigned char addr, float *pitch,float *roll,float *yaw);
void run_self_test(void);

//ALIENTEK 探索者STM32F407开发板 实验32
//MPU6050六轴传感器 实验 -库函数版本
//技术支持：www.openedv.com
//淘宝店铺：http://eboard.taobao.com  
//广州市星翼电子科技有限公司  
//作者：正点原子 @ALIENTEK

//串口1发送1个字符 
//c:要发送的字符
void usart1_send_char(u8 c)
{

	while(USART_GetFlagStatus(USART1,USART_FLAG_TC)==RESET); 
    USART_SendData(USART1,c);   

} 
//传送数据给匿名四轴上位机软件(V2.6版本)
//fun:功能字. 0XA0~0XAF
//data:数据缓存区,最多28字节!!
//len:data区有效数据个数
void usart1_niming_report(u8 fun,u8*data,u8 len)
{
	u8 send_buf[32];
	u8 i;
	if(len>28)return;	//最多28字节数据 
	send_buf[len+3]=0;	//校验数置零
	send_buf[0]=0X88;	//帧头
	send_buf[1]=fun;	//功能字
	send_buf[2]=len;	//数据长度
	for(i=0;i<len;i++)send_buf[3+i]=data[i];			//复制数据
	for(i=0;i<len+3;i++)send_buf[len+3]+=send_buf[i];	//计算校验和	
	for(i=0;i<len+4;i++)usart1_send_char(send_buf[i]);	//发送数据到串口1 
}
//发送加速度传感器数据和陀螺仪数据
//aacx,aacy,aacz:x,y,z三个方向上面的加速度值
//gyrox,gyroy,gyroz:x,y,z三个方向上面的陀螺仪值
void mpu6050_send_data(short aacx,short aacy,short aacz,short gyrox,short gyroy,short gyroz)
{
	u8 tbuf[12]; 
	tbuf[0]=(aacx>>8)&0XFF;
	tbuf[1]=aacx&0XFF;
	tbuf[2]=(aacy>>8)&0XFF;
	tbuf[3]=aacy&0XFF;
	tbuf[4]=(aacz>>8)&0XFF;
	tbuf[5]=aacz&0XFF; 
	tbuf[6]=(gyrox>>8)&0XFF;
	tbuf[7]=gyrox&0XFF;
	tbuf[8]=(gyroy>>8)&0XFF;
	tbuf[9]=gyroy&0XFF;
	tbuf[10]=(gyroz>>8)&0XFF;
	tbuf[11]=gyroz&0XFF;
	usart1_niming_report(0XA1,tbuf,12);//自定义帧,0XA1
}	
//通过串口1上报结算后的姿态数据给电脑
//aacx,aacy,aacz:x,y,z三个方向上面的加速度值
//gyrox,gyroy,gyroz:x,y,z三个方向上面的陀螺仪值
//roll:横滚角.单位0.01度。 -18000 -> 18000 对应 -180.00  ->  180.00度
//pitch:俯仰角.单位 0.01度。-9000 - 9000 对应 -90.00 -> 90.00 度
//yaw:航向角.单位为0.1度 0 -> 3600  对应 0 -> 360.0度
void usart1_report_imu(short aacx,short aacy,short aacz,short gyrox,short gyroy,short gyroz,short roll,short pitch,short yaw)
{
	u8 tbuf[28]; 
	u8 i;
	for(i=0;i<28;i++)tbuf[i]=0;//清0
	tbuf[0]=(aacx>>8)&0XFF;
	tbuf[1]=aacx&0XFF;
	tbuf[2]=(aacy>>8)&0XFF;
	tbuf[3]=aacy&0XFF;
	tbuf[4]=(aacz>>8)&0XFF;
	tbuf[5]=aacz&0XFF; 
	tbuf[6]=(gyrox>>8)&0XFF;
	tbuf[7]=gyrox&0XFF;
	tbuf[8]=(gyroy>>8)&0XFF;
	tbuf[9]=gyroy&0XFF;
	tbuf[10]=(gyroz>>8)&0XFF;
	tbuf[11]=gyroz&0XFF;	
	tbuf[18]=(roll>>8)&0XFF;
	tbuf[19]=roll&0XFF;
	tbuf[20]=(pitch>>8)&0XFF;
	tbuf[21]=pitch&0XFF;
	tbuf[22]=(yaw>>8)&0XFF;
	tbuf[23]=yaw&0XFF;
	usart1_niming_report(0XAF,tbuf,28);//飞控显示帧,0XAF
} 
  
int main(void)
{
  u8 ret;	
	u8 t=0,report=0;			//默认开启上报
	u8 key;
	float pitch,roll,yaw; 		//欧拉角
	short aacx,aacy,aacz;		//加速度传感器原始数据
	short gyrox,gyroy,gyroz;	//陀螺仪原始数据
	short temp;					//温度
    
	float pitch1,roll1,yaw1; 		//欧拉角
	short aacx1,aacy1,aacz1;		//加速度传感器原始数据
	short gyrox1,gyroy1,gyroz1;	//陀螺仪原始数据
	short temp1;					//温度
    
	float pitch2,roll2,yaw2; 		//欧拉角
	short aacx2,aacy2,aacz2;		//加速度传感器原始数据
	short gyrox2,gyroy2,gyroz2;	//陀螺仪原始数据
	short temp2;					//温度
    
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置系统中断优先级分组2
	delay_init(168);  //初始化延时函数
	uart_init(115200);		//初始化串口波特率为500000
//	LED_Init();					//初始化LED 
//	KEY_Init();					//初始化按键
 	LCD_Init();					//LCD初始化
	MPU_SPI_Init();
//    delay_ms(1000);
	ret = MPU_Init(MPU_CS_NUM_1);					//初始化MPU6050
	while(ret){
		printf("Err MPU_Init MPU_CS_NUM_1\r\n");
		delay_ms(500);
	}
	ret = MPU_Init(MPU_CS_NUM_2);					//初始化MPU6050
	while(ret){
		printf("Err MPU_Init MPU_CS_NUM_2\r\n");
		delay_ms(500);
	}
 	POINT_COLOR=RED;//设置字体为红色 
	do{
		printf("hello world from printf!!!\r\n");
	}while(0);
	LCD_ShowString(30,50,200,16,16,"Explorer STM32F4");	
	LCD_ShowString(30,70,200,16,16,"MPU6050 TEST");	
	LCD_ShowString(30,90,200,16,16,"ATOM@ALIENTEK");
	LCD_ShowString(30,110,200,16,16,"2014/5/9");
	while(mpu_dmp_init())
	{
		LCD_ShowString(30,130,200,16,16,"MPU6050 Error");
		delay_ms(200);
		LCD_Fill(30,130,239,130+16,WHITE);
 		delay_ms(200);
	}
	LCD_ShowString(30,130,200,16,16,"MPU6050 OK");
	LCD_ShowString(30,150,200,16,16,"KEY0:UPLOAD ON/OFF");
	POINT_COLOR=BLUE;//设置字体为蓝色 
 	LCD_ShowString(30,170,200,16,16,"UPLOAD ON ");	 
 	LCD_ShowString(30,200,200,16,16," Temp:    . C");	
 	LCD_ShowString(30,220,200,16,16,"Pitch:    . C");	
 	LCD_ShowString(30,240,200,16,16," Roll:    . C");	 
 	LCD_ShowString(30,260,200,16,16," Yaw :    . C");
	
  unsigned int counter;  
 	while(1)
	{
#if 1
		if(mpu_dmp_get_data(MPU_CS_NUM_1,&pitch1,&roll1,&yaw1)==0)
			counter++;
		if(mpu_dmp_get_data(MPU_CS_NUM_2,&pitch2,&roll2,&yaw2)==0)
			counter++;
		
		if(counter > 150){
			counter = 0;
			printf("pitch1=%f, roll1=%f, yaw1=%f\r\n", pitch1,roll1,yaw1);
			printf("pitch2=%f, roll2=%f, yaw2=%f\r\n", pitch2,roll2,yaw2);
		}
//delay_ms(100);        
#endif
#if 0
		if(mpu_dmp_get_data(MPU_CS_NUM_1,&pitch,&roll,&yaw)==0)
		{
			printf("pitch=%f, roll=%f, yaw=%f\r\n", pitch,roll,yaw);
			
			int ret;
			long temperature;
			short accel[3];
			short gyro[3];
			
			ret = mpu_get_temperature(MPU_CS_NUM_1, &temperature, NULL);
			if(ret)
				printf("Err call mpu_get_temperature, Err ret = %d\r\n", ret);
			printf("temperature=%ld\r\n", temperature);
			ret = mpu_get_accel_reg(MPU_CS_NUM_1, accel, NULL);
			if(ret)
				printf("Err call mpu_get_accel_reg, Err ret = %d\r\n", ret);
			printf("accel[0]=%d, accel[1]=%d, accel[2]=%d\r\n", accel[0],accel[1],accel[2]);
			ret = mpu_get_gyro_reg(MPU_CS_NUM_1, gyro, NULL);
			if(ret)
				printf("Err call mpu_get_gyro_reg, Err ret = %d\r\n", ret);
			printf("gyro[0]=%d, gyro[1]=%d, gyro[2]=%d\r\n", gyro[0],gyro[1],gyro[2]);
			
			temp=MPU_Get_Temperature(MPU_CS_NUM_1);	//得到温度值
			printf("MPU6500 temperature=%ld\r\n", temperature);
			MPU_Get_Accelerometer(MPU_CS_NUM_1, &aacx,&aacy,&aacz);	//得到加速度传感器数据
			printf("MPU6500 aacx=%d, aacy=%d, aacz=%d\r\n", aacx,aacy,aacz);
			MPU_Get_Gyroscope(MPU_CS_NUM_1,&gyrox,&gyroy,&gyroz);	//得到陀螺仪数据
			printf("MPU6500 gyrox=%d, gyroy=%d, gyroz=%d\r\n", gyrox,gyroy,gyroz);
			if((t%10)==0)
			{ 
				if(temp<0)
				{
					LCD_ShowChar(30+48,200,'-',16,0);		//显示负号
					temp=-temp;		//转为正数
				}else LCD_ShowChar(30+48,200,' ',16,0);		//去掉负号 
				LCD_ShowNum(30+48+8,200,temp/100,3,16);		//显示整数部分	    
				LCD_ShowNum(30+48+40,200,temp%10,1,16);		//显示小数部分 
				temp=pitch*10;
				if(temp<0)
				{
					LCD_ShowChar(30+48,220,'-',16,0);		//显示负号
					temp=-temp;		//转为正数
				}else LCD_ShowChar(30+48,220,' ',16,0);		//去掉负号 
				LCD_ShowNum(30+48+8,220,temp/10,3,16);		//显示整数部分	    
				LCD_ShowNum(30+48+40,220,temp%10,1,16);		//显示小数部分 
				temp=roll*10;
				if(temp<0)
				{
					LCD_ShowChar(30+48,240,'-',16,0);		//显示负号
					temp=-temp;		//转为正数
				}else LCD_ShowChar(30+48,240,' ',16,0);		//去掉负号 
				LCD_ShowNum(30+48+8,240,temp/10,3,16);		//显示整数部分	    
				LCD_ShowNum(30+48+40,240,temp%10,1,16);		//显示小数部分 
				temp=yaw*10;
				if(temp<0)
				{
					LCD_ShowChar(30+48,260,'-',16,0);		//显示负号
					temp=-temp;		//转为正数
				}else LCD_ShowChar(30+48,260,' ',16,0);		//去掉负号 
				LCD_ShowNum(30+48+8,260,temp/10,3,16);		//显示整数部分	    
				LCD_ShowNum(30+48+40,260,temp%10,1,16);		//显示小数部分  
				t=0;
				LED0=!LED0;//LED闪烁
			}
		}
		t++; 
#endif
	} 	
}

#define DEFAULT_MPU_HZ  (200)
#define ACCEL_ON        (0x01)
#define GYRO_ON         (0x02)
struct platform_data_s {
    signed char orientation[9];
};
static struct platform_data_s gyro_pdata = {
    .orientation = { 1, 0, 0,
                     0, 1, 0,
                     0, 0, 1}
};
struct rx_s {
    unsigned char header[3];
    unsigned char cmd;
};
struct hal_s {
    unsigned char lp_accel_mode;
    unsigned char sensors;
    unsigned char dmp_on;
    unsigned char wait_for_tap;
    volatile unsigned char new_gyro;
    unsigned char motion_int_mode;
    unsigned long no_dmp_hz;
    unsigned long next_pedo_ms;
    unsigned long next_temp_ms;
    unsigned long next_compass_ms;
    unsigned int report;
    unsigned short dmp_features;
    struct rx_s rx;
};
static struct hal_s hal = {0};
uint8_t mpu_dmp_init(void)
{
	int result;
	struct int_param_s int_param[MPU_CS_NUM_MAX];
	unsigned char accel_fsr,  new_temp = 0;
	unsigned short gyro_rate, gyro_fsr;
	
	
  result = mpu_init(MPU_CS_NUM_1, &int_param[MPU_CS_NUM_1]);
  if (result) {
		LCD_ShowString(30,500,200,16,16,"Could not initialize gyro.");
		return 1;
  }
  result = mpu_init(MPU_CS_NUM_2, &int_param[MPU_CS_NUM_1]);
  if (result) {
		LCD_ShowString(30,600,200,16,16,"Could not initialize gyro.");
		return 1;
  }  
    result = mpu_set_sensors(MPU_CS_NUM_1, INV_XYZ_GYRO | INV_XYZ_ACCEL);
  if (result) {
		LCD_ShowString(30,520,200,16,16,"mpu_set_sensors.");
		return 1;
  }
    result = mpu_set_sensors(MPU_CS_NUM_2, INV_XYZ_GYRO | INV_XYZ_ACCEL);
  if (result) {
		LCD_ShowString(30,620,200,16,16,"mpu_set_sensors.");
		return 1;
  }
	delay_ms(1);
  
	result = mpu_configure_fifo(MPU_CS_NUM_1, INV_XYZ_GYRO | INV_XYZ_ACCEL);
  if (result) {
		LCD_ShowString(30,520,200,16,16,"mpu_configure_fifo.");
		return 1;
  }
	result = mpu_configure_fifo(MPU_CS_NUM_2, INV_XYZ_GYRO | INV_XYZ_ACCEL);
  if (result) {
		LCD_ShowString(30,620,200,16,16,"mpu_configure_fifo.");
		return 1;
  }  
	delay_ms(1);
  
	result = mpu_set_sample_rate(MPU_CS_NUM_1, DEFAULT_MPU_HZ);
  if (result) {
		LCD_ShowString(30,520,200,16,16,"mpu_set_sample_rate.");
		return 1;
  }
	result = mpu_set_sample_rate(MPU_CS_NUM_2, DEFAULT_MPU_HZ);
  if (result) {
		LCD_ShowString(30,620,200,16,16,"mpu_set_sample_rate.");
		return 1;
  }
	delay_ms(1);

	hal.sensors = ACCEL_ON | GYRO_ON;
	hal.dmp_on = 0;
	hal.report = 0;
	hal.rx.cmd = 0;
	hal.next_pedo_ms = 0;
	hal.next_compass_ms = 0;
	hal.next_temp_ms = 0;
	
    result = dmp_load_motion_driver_firmware(MPU_CS_NUM_1);
		if (result) {
			LCD_ShowString(30,520,300,16,16,"dmp_load_motion_driver_firmware.");
			return 1;
		}
    result = dmp_load_motion_driver_firmware(MPU_CS_NUM_2);
		if (result) {
			LCD_ShowString(30,620,300,16,16,"dmp_load_motion_driver_firmware.");
			return 1;
		}	delay_ms(1);
    dmp_set_orientation(MPU_CS_NUM_1, inv_orientation_matrix_to_scalar(gyro_pdata.orientation));
    dmp_set_orientation(MPU_CS_NUM_2, inv_orientation_matrix_to_scalar(gyro_pdata.orientation));
	delay_ms(1);

    hal.dmp_features = DMP_FEATURE_6X_LP_QUAT | DMP_FEATURE_TAP |
        DMP_FEATURE_ANDROID_ORIENT | DMP_FEATURE_SEND_RAW_ACCEL | DMP_FEATURE_SEND_CAL_GYRO |
        DMP_FEATURE_GYRO_CAL;
    dmp_enable_feature(MPU_CS_NUM_1, hal.dmp_features);
    dmp_enable_feature(MPU_CS_NUM_2, hal.dmp_features);
	delay_ms(1);
    dmp_set_fifo_rate(MPU_CS_NUM_1, DEFAULT_MPU_HZ);
    dmp_set_fifo_rate(MPU_CS_NUM_2, DEFAULT_MPU_HZ);
	delay_ms(1);
		
    mpu_set_dmp_state(MPU_CS_NUM_1, 1);
    mpu_set_dmp_state(MPU_CS_NUM_2, 1);
    hal.dmp_on = 1;


	return 0;
}

#define q30  1073741824.0f
uint8_t mpu_dmp_get_data(unsigned char addr, float *pitch,float *roll,float *yaw)
{
	float q0=1.0f,q1=0.0f,q2=0.0f,q3=0.0f;
	unsigned long sensor_timestamp;
	short gyro[3], accel[3], sensors;
	unsigned char more;
	long quat[4]; 
	if(dmp_read_fifo(addr, gyro, accel, quat, &sensor_timestamp, &sensors,&more))return 1;	 
	/* Gyro and accel data are written to the FIFO by the DMP in chip frame and hardware units.
	 * This behavior is convenient because it keeps the gyro and accel outputs of dmp_read_fifo and mpu_read_fifo consistent.
	**/
	/*if (sensors & INV_XYZ_GYRO )
	send_packet(PACKET_TYPE_GYRO, gyro);
	if (sensors & INV_XYZ_ACCEL)
	send_packet(PACKET_TYPE_ACCEL, accel); */
	/* Unlike gyro and accel, quaternions are written to the FIFO in the body frame, q30.
	 * The orientation is set by the scalar passed to dmp_set_orientation during initialization. 
	**/
	if(sensors&INV_WXYZ_QUAT) 
	{
		q0 = quat[0] / q30;	//q30格式转换为浮点数
		q1 = quat[1] / q30;
		q2 = quat[2] / q30;
		q3 = quat[3] / q30; 
		//计算得到俯仰角/横滚角/航向角
		*pitch = asin(-2 * q1 * q3 + 2 * q0* q2)* 57.3;	// pitch
		*roll  = atan2(2 * q2 * q3 + 2 * q0 * q1, -2 * q1 * q1 - 2 * q2* q2 + 1)* 57.3;	// roll
		*yaw   = atan2(2*(q1*q2 + q0*q3),q0*q0+q1*q1-q2*q2-q3*q3) * 57.3;	//yaw
	}else return 2;
	
  return 0;
}


#define MPL_LOGI printf
#define MPL_LOGE printf
